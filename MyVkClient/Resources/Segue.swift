//
//  Segue.swift
//  MyVkClient
//
//  Created by Artyom on 23.10.2020.
//  Copyright © 2020 Artyom. All rights reserved.
//

import Foundation

class Segue {
    
    static let showAuthorizationSegue = "showAuthorizationSegue"
    static let showFriendsSegue = "showFriendsSegue"
    static let showFriendAfterSignInSegue = "showFriendAfterSignInSegue"
}
