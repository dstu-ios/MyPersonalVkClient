//
// Created by Artyom on 14.11.2020.
// Copyright (c) 2020 Artyom. All rights reserved.
//

import Foundation

class BaseNetworkManager {

    private let jwtToken: JwtToken

    typealias ResponseType = (Data, VkErrorResponse?) -> ()

    init(jwtToken: JwtToken) {
        self.jwtToken = jwtToken
    }

    func httpCall(method: String, args: [String: Any]?, callback: @escaping ResponseType) {

        var urlString = getUrl(name: method)

        if let args = args {
            urlString += args.map { "&\($0)=\($1)" }.joined()
        }

        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"

        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                return
            }

            // Log Vk response
            debugPrint(String(data: data, encoding: .utf8)!)

            let vkError = JsonMapper.map(VkErrorResponse.self, from: data)

            DispatchQueue.main.async {
                callback(data, vkError)
            }
        }.resume()
    }

    func getUrl(name methodName: String) -> String {
        "https://api.vk.com/method/\(methodName)?v=\(API_VERSION)&access_token=\(jwtToken.accessToken)"
    }
}