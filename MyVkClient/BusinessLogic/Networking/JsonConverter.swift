//
//  JsonConverter.swift
//  MyVkClient
//
//  Created by Artyom on 24.10.2020.
//  Copyright © 2020 Artyom. All rights reserved.
//

import Foundation



func convertToDictionary(data: Data) -> [String: Any]? {
    
    do {
        return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
    } catch {
        print(error.localizedDescription)
    }

    return nil
}


class JsonMapper {

    private static let decoder = JSONDecoder()

    static func map<T>(_ type: T.Type, from jsonData: Data) -> T? where T: Decodable {
        do {
            return try decoder.decode(type, from: jsonData)
        } catch (let e) {
            debugPrint(e)
            return nil
        }
    }

    static func map<T>(_ request: T) -> [String: Any]? where T: Encodable {
        do {
            let data = try JSONEncoder().encode(request)
            let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            return json
        } catch (let e) {
            debugPrint(e)
            return nil
        }
    }
}
