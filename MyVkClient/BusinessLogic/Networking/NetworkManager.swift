//
//  NetworkManaher.swift
//  MyVkClient
//
//  Created by Artyom on 24.10.2020.
//  Copyright © 2020 Artyom. All rights reserved.
//

import Foundation

class NetworkManager: BaseNetworkManager {
    
    func getFriends(callback: @escaping (VKResponse?, String?) -> ()) {

        let method = "friends.get"

        var arguments: [String: Any] = [:]
        arguments["fields"] = "city,domain,online"

        httpCall(method: method, args: arguments) { data, vkError in
            if let error = vkError {
                callback(nil, error.error.errorMsg)
            } else {
                let vkResponse = JsonMapper.map(VKResponse.self, from: data)
                callback(vkResponse, nil)
            }
        }
    }

    func getConversations(count: Int, offset: Int, callback: @escaping (VKMessageResponse?, String?) -> ()) {
        // https://vk.com/dev/messages.getConversations

        let method = "messages.getConversations"

        var arguments: [String: Any] = [:]
        arguments["count"] = count
        arguments["offset"] = offset
        arguments["extended"] = 1
        arguments["fields"] = "city,domain,online,has_photo,photo,crop_photo,photo_50,chat_settings"

        httpCall(method: method, args: arguments) { data, vkError in
            if let error = vkError {
                callback(nil, error.error.errorMsg)
            } else {
                let vkResponse = JsonMapper.map(VKMessageResponse.self, from: data)
                callback(vkResponse, nil)
            }
        }
    }

    static func downloadPic(urlString: String, callback: @escaping (Data) -> ()) {
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"

        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else {
                return
            }
            callback(data)
        }.resume()
    }
}
