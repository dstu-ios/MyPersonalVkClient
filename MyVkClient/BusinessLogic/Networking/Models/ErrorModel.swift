//
// Created by Artyom on 14.11.2020.
// Copyright (c) 2020 Artyom. All rights reserved.
//

import Foundation

struct VkErrorResponse: Codable {
    let error: VkErrorModel
}

// MARK: - Error
struct VkErrorModel: Codable {
    let errorCode: Int
    let errorMsg: String
    let requestParams: [RequestParam]

    enum CodingKeys: String, CodingKey {
        case errorCode = "error_code"
        case errorMsg = "error_msg"
        case requestParams = "request_params"
    }
}

// MARK: - RequestParam
struct RequestParam: Codable {
    let key, value: String
}