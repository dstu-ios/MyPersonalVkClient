//
//  VKMessage.swift
//  MyVkClient
//
//  Created by Artyom on 07.11.2020.
//  Copyright © 2020 Artyom. All rights reserved.
//

import Foundation

// MARK: - Welcome
struct VKMessageResponse: Codable {
    let response: VKMessagesPayload
}

// MARK: - Response
struct VKMessagesPayload: Codable {
    let count: Int
    let items: [VKMessage]
    let profiles: [VkProfile]
}

// MARK: - Item
struct VKMessage: Codable {
    let conversation: VKConversation
    let lastMessage: VKLastMessage

    enum CodingKeys: String, CodingKey {
        case conversation
        case lastMessage = "last_message"
    }
}

// MARK: - Conversation
struct VKConversation: Codable {
    let peer: Peer
    let lastMessageID, inRead, outRead: Int
    let sortID: SortID?
    let isMarkedUnread, important: Bool
    let pushSettings: PushSettings?
    let canWrite: CanWrite
    let chatSettings: VKChatSettings?

    enum CodingKeys: String, CodingKey {
        case peer
        case lastMessageID = "last_message_id"
        case inRead = "in_read"
        case outRead = "out_read"
        case sortID = "sort_id"
        case isMarkedUnread = "is_marked_unread"
        case important
        case pushSettings = "push_settings"
        case canWrite = "can_write"
        case chatSettings = "chat_settings"
    }
}


// MARK: - CanWrite
struct CanWrite: Codable {
    let allowed: Bool
}

// MARK: - ChatSettings
struct VKChatSettings: Codable {
    let ownerID: Int
    let title, state: String
    let acl: ACL?
    let membersCount: Int
    let activeIDS: [Int]
    let isGroupChannel, isDisappearing, isService: Bool?

    enum CodingKeys: String, CodingKey {
        case ownerID = "owner_id"
        case title, state, acl
        case membersCount = "members_count"
        case activeIDS = "active_ids"
        case isGroupChannel = "is_group_channel"
        case isDisappearing = "is_disappearing"
        case isService = "is_service"
    }
}

// MARK: - ACL
struct ACL: Codable {
    let canChangeInfo, canChangeInviteLink, canChangePin, canInvite: Bool
    let canPromoteUsers, canSeeInviteLink, canModerate, canCopyChat: Bool
    let canCall, canUseMassMentions, canChangeServiceType: Bool

    enum CodingKeys: String, CodingKey {
        case canChangeInfo = "can_change_info"
        case canChangeInviteLink = "can_change_invite_link"
        case canChangePin = "can_change_pin"
        case canInvite = "can_invite"
        case canPromoteUsers = "can_promote_users"
        case canSeeInviteLink = "can_see_invite_link"
        case canModerate = "can_moderate"
        case canCopyChat = "can_copy_chat"
        case canCall = "can_call"
        case canUseMassMentions = "can_use_mass_mentions"
        case canChangeServiceType = "can_change_service_type"
    }
}

// MARK: - Peer
struct Peer: Codable {
    let id: Int
    let type: String
    let localID: Int

    enum CodingKeys: String, CodingKey {
        case id, type
        case localID = "local_id"
    }
}

// MARK: - PushSettings
struct PushSettings: Codable {
    let disabledForever, noSound: Bool

    enum CodingKeys: String, CodingKey {
        case disabledForever = "disabled_forever"
        case noSound = "no_sound"
    }
}

// MARK: - SortID
struct SortID: Codable {
    let majorID, minorID: Int

    enum CodingKeys: String, CodingKey {
        case majorID = "major_id"
        case minorID = "minor_id"
    }
}

// MARK: - LastMessage
struct VKLastMessage: Codable {
    
    let date, fromID, id, out: Int
    let peerID: Int
    let text: String
    let conversationMessageID: Int

    let important: Bool
    let randomID: Int
    let isHidden: Bool

    let action: VkChatAction?

    enum CodingKeys: String, CodingKey {
        case date
        case action
        case fromID = "from_id"
        case id, out
        case peerID = "peer_id"
        case text
        case conversationMessageID = "conversation_message_id"
        case important
        case randomID = "random_id"
        case isHidden = "is_hidden"
    }
}

// MARK: - Action
struct VkChatAction: Codable {
    let type: String
    let memberID: Int

    enum CodingKeys: String, CodingKey {
        case type
        case memberID = "member_id"
    }
}

struct VkProfile: Codable {
    let firstName: String
    let id: Int
    let lastName: String
    let photo: String?

    enum CodingKeys: String, CodingKey {
        case firstName = "first_name"
        case id
        case lastName = "last_name"
        case photo
    }
}