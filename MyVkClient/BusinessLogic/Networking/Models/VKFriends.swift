//
//  VKFriends.swift
//  MyVkClient
//
//  Created by Artyom on 31.10.2020.
//  Copyright © 2020 Artyom. All rights reserved.
//

import Foundation


// MARK: - VKResponse
struct VKResponse: Codable {
    let response: VKPayload
}

// MARK: - VKPayload
struct VKPayload: Codable {
    let count: Int
    let items: [VKFriend]
}

// MARK: - VKFriend
struct VKFriend: Codable {
    let id: Int
    let firstName, lastName: String
    let isClosed, canAccessClosed: Bool?
    let domain: String
    let city: VKCity?
    let online: Int
    let trackCode: String

    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case isClosed = "is_closed"
        case canAccessClosed = "can_access_closed"
        case domain, city, online
        case trackCode = "track_code"
    }
}

// MARK: - City
struct VKCity: Codable {
    let id: Int
    let title: String
}
