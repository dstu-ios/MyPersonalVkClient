//
//  AuthManager.swift
//  MyVkClient
//
//  Created by Artyom on 22.10.2020.
//  Copyright © 2020 Artyom. All rights reserved.
//

import Foundation
import UIKit

let API_VERSION = "5.124"

let clientId = 7635837
let display = "mobile"
let redirect_uri = "https://oauth.vk.com/blank.html"
let scope = "friends,messages"
let response_type = "token"

let sharedKey = "myVkAccessTokenKey"

func getSignInUrl() -> URL {

    let urlString = "https://oauth.vk.com/authorize?client_id=\(clientId)&display=\(display)&redirect_uri=\(redirect_uri)&scope=\(scope)&response_type=\(response_type)&v=\(API_VERSION)"

    return URL(string: urlString)!
}
