//
// Created by Artyom on 23.10.2020.
// Copyright (c) 2020 Artyom. All rights reserved.
//

import Foundation

class JwtToken: Codable {
    
    static let JWT_TOKEN_KEY = "JWT_TOKEN_KEY"
    
    let userId: Int
    let expiresIn: Int
    let accessToken: String
    let createdAt: Date

    init(user_id: Int, expires_in: Int, access_token: String) {
        self.userId = user_id
        self.expiresIn = expires_in
        self.accessToken = access_token
        self.createdAt = Date()
    }

    var isAlive: Bool {
        let component = DateComponents(second: self.expiresIn)
        let now = Date()
        return now < Calendar.current.date(byAdding: component, to: self.createdAt)!
    }

    static func create(from redurectedUrlString: String) {
        // http://REDIRECT_URI#access_token=533bacf01e11f55b536a565b57531ad114461ae8736d6506a3&expires_in=86400&user_id=8492&state=123456
        
        
        let payloadString: String = String(redurectedUrlString.split(separator: "#")[1])
        
        var accessTokenMapping = [String: String]()
        for keyValueStr in payloadString.split(separator: "&") {
            let keyValueArr = keyValueStr.split(separator: "=")
            
            let key: String = String(keyValueArr[0])
            let value: String = String(keyValueArr[1])
            
            accessTokenMapping[key] = value
        }

        let userId = Int(accessTokenMapping["user_id"]!)!
        let expiresIn = Int(accessTokenMapping["expires_in"]!)!
        let accessToken = accessTokenMapping["access_token"]!

        let jwtToken = JwtToken(user_id: userId, expires_in: expiresIn, access_token: accessToken)
        jwtToken.save()
    }
    
    static func load() -> JwtToken? {
        let defaults = UserDefaults.standard
        
        guard let data = defaults.data(forKey: JwtToken.JWT_TOKEN_KEY) else {
            return nil
        }
        
        let decoder = JSONDecoder()
        return try! decoder.decode(JwtToken.self, from: data)
    }
    
    func save() {
        let data = try! JSONEncoder().encode(self)
        
        let defaults = UserDefaults.standard
        defaults.set(data, forKey: JwtToken.JWT_TOKEN_KEY)
    }
}
