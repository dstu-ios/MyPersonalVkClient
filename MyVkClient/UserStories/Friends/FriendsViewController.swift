//
// Created by Artyom on 31.10.2020.
// Copyright (c) 2020 Artyom. All rights reserved.
//

import Foundation
import UIKit

class FriendsViewController: UITableViewController {

    private var vkPayload: VKPayload?

    override func viewDidLoad() {
        super.viewDidLoad()

        prepareTableView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        loadFriends()
    }

    private func prepareTableView() {
        tableView.delegate = self
        tableView.dataSource = self
    }

    private func loadFriends() {
        let token = JwtToken.load()!
        let manager = NetworkManager(jwtToken: token)

        manager.getFriends() { [weak self] vkResponse, errorMessage in

            DispatchQueue.main.async{
                if let vkResponse = vkResponse {
                    self?.vkPayload = vkResponse.response
                    self?.tableView.reloadData()
                }
            }
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.vkPayload?.items.count ?? 0
    }

    // create a cell for each table view row
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        // create a new cell if needed or reuse an old one
        let cell = self.tableView.dequeueReusableCell(withIdentifier: Cells.uiFriendTableViewCell) as! UIFriendTableViewCell

        // set the text from the data model
        let friend = vkPayload!.items[indexPath.row]

        cell.name?.text = "\(friend.firstName) \(friend.lastName)"

        return cell
    }

    // method to run when table view cell is tapped
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }

}
