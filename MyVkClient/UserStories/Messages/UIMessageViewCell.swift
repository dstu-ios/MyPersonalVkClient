//
// Created by Artyom on 14.11.2020.
// Copyright (c) 2020 Artyom. All rights reserved.
//

import Foundation
import UIKit

class UIMessageViewCell: UITableViewCell {
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var userPicImageView: UIImageView!

    var message: VKMessage?
    var profiles: [VkProfile]?

    func bake() {
        guard let message = self.message, let profiles = self.profiles else {
            return
        }

        messageLabel.text = resolveTextMessage(message: message.lastMessage)
        titleLable.text = resolveMessageTitle(message: message, profiles: profiles)

        resolveUserPic(message: message, profiles: profiles)
    }

    func resolveTextMessage(message: VKLastMessage) -> String {
        if message.text != "" {
            return message.text
        } else if message.action?.type == "chat_kick_user" {
            return "Пользователь покинул беседу."
        } else {
            return "Сообщение"
        }
    }

    func resolveMessageTitle(message: VKMessage, profiles: [VkProfile]) -> String {
        if message.conversation.peer.type == "user" {
            return profiles[0].firstName
        } else if message.conversation.peer.type == "chat" {
            return message.conversation.chatSettings!.title
        } else {
            return "Nothing"
        }
    }

    func resolveUserPic(message: VKMessage, profiles: [VkProfile]) {
        if message.conversation.peer.type == "user" {
            guard let profile = profiles.filter({ $0.id == message.conversation.peer.id}).first, let photo = profile.photo else {
                return
            }
            downloadUserPic(urlString: photo)
        }
    }

    func downloadUserPic(urlString: String) {

        NetworkManager.downloadPic(urlString: urlString) { [weak self] data in
            DispatchQueue.main.async {
                self?.userPicImageView.image = UIImage(data: data)
            }
        }
    }
}
