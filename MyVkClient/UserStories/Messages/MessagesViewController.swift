//
//  MessagesViewController.swift
//  MyVkClient
//
//  Created by Artyom on 07.11.2020.
//  Copyright © 2020 Artyom. All rights reserved.
//

import Foundation
import UIKit

class MessagesViewController: UIViewController {

    @IBOutlet weak var messagesTableView: UITableView!

    private var messagesPayload: VKMessagesPayload?

    override func viewDidLoad() {
        super.viewDidLoad()

        prepareTableView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        loadMessages()
    }

    private func loadMessages() {

        let token = JwtToken.load()!
        let manager = NetworkManager(jwtToken: token)

        manager.getConversations(count: 10, offset: 0) { [weak self] vkResponse, errorMessage in
            if let vkResponse = vkResponse {
                self?.messagesPayload = vkResponse.response

                self?.messagesTableView.reloadData()
            }
        }
    }

    private func prepareTableView() {
        messagesTableView.delegate = self
        messagesTableView.dataSource = self
    }
}

extension MessagesViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.messagesPayload?.items.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: Cells.uiConversationsTableViewCell) as! UIMessageViewCell

        let message = messagesPayload!.items[indexPath.row]

        cell.message = message
        cell.profiles = messagesPayload!.profiles

        cell.bake()

        return cell
    }
}
