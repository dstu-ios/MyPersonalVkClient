//
//  ViewController.swift
//  MyVkClient
//
//  Created by Artyom on 22.10.2020.
//  Copyright © 2020 Artyom. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var signInButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareSignInButton()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let jwt = JwtToken.load(), jwt.isAlive {
            performSegue(withIdentifier: Segue.showFriendsSegue, sender: nil)
        }
    }

    @IBAction func didTapSignInButton(_ sender: Any) {
        performSegue(withIdentifier: Segue.showAuthorizationSegue, sender: nil)
    }

    private func prepareSignInButton() {
        signInButton.layer.cornerRadius = 15
    }
}
