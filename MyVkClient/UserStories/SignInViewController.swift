//
//  SignInController.swift
//  MyVkClient
//
//  Created by Artyom on 23.10.2020.
//  Copyright © 2020 Artyom. All rights reserved.
//

import Foundation
import UIKit
import WebKit


class SignInViewController: UIViewController {
    
    
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareWebView()
    }
    
    private func prepareWebView() {
        let url = getSignInUrl()
        webView.load(URLRequest(url: url))

        webView.navigationDelegate = self
    }
    
    private func showFriendsController() {
        performSegue(
            withIdentifier: Segue.showFriendAfterSignInSegue, sender: nil
        )
    }
}

extension SignInViewController: WKNavigationDelegate {

    func webView(
            _ webView: WKWebView,
            decidePolicyFor navigationAction: WKNavigationAction,
            decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void) {

        if webView.url!.absoluteString.starts(with: "https://oauth.vk.com/blank.html") {
            let urlString = webView.url!.absoluteString

            JwtToken.create(from: urlString)
            showFriendsController()
            
            decisionHandler(.cancel)
            return
        }
        decisionHandler(.allow)
    }
}
